# UPDATE APT CACHE TO POINT TO ARCHIVE
echo 'deb http://old-releases.ubuntu.com/ubuntu/ utopic main restricted' > /etc/apt/sources.list
echo 'deb-src http://old-releases.ubuntu.com/ubuntu/ utopic main restricted' >> /etc/apt/sources.list
echo 'deb http://old-releases.ubuntu.com/ubuntu/ utopic-updates main restricted' >> /etc/apt/sources.list
echo 'deb-src http://old-releases.ubuntu.com/ubuntu/ utopic-updates main restricted' >> /etc/apt/sources.list
echo 'deb http://old-releases.ubuntu.com/ubuntu/ utopic universe' >> /etc/apt/sources.list
echo 'deb-src http://old-releases.ubuntu.com/ubuntu/ utopic universe' >> /etc/apt/sources.list
echo 'deb http://old-releases.ubuntu.com/ubuntu/ utopic-updates universe' >> /etc/apt/sources.list
echo 'deb-src http://old-releases.ubuntu.com/ubuntu/ utopic-updates universe' >> /etc/apt/sources.list
echo 'deb http://old-releases.ubuntu.com/ubuntu/ utopic-security main restricted' >> /etc/apt/sources.list
echo 'deb-src http://old-releases.ubuntu.com/ubuntu/ utopic-security main restricted' >> /etc/apt/sources.list
echo 'deb http://old-releases.ubuntu.com/ubuntu/ utopic-security universe' >> /etc/apt/sources.list
echo 'deb-src http://old-releases.ubuntu.com/ubuntu/ utopic-security universe' >> /etc/apt/sources.list

apt-get update

dpkg --add-architecture i386
apt-get update

echo 'y' | apt-get install libc6:i386 g++-multilib gcc-multilib libconfig++
echo 'y' | apt-get install gcc make g++ zlib1g-dev libssl-dev libbz2-dev git wget bzip2 udev vim libsqlite3-dev subversion mercurial libtool libconfig++ unzip

# PYTHON
git clone git://github.com/yyuu/pyenv.git /pyenv

echo 'ln -s /proc/self/fd /dev/fd'  >> /etc/bash.bashrc
echo 'export PYENV_ROOT=/pyenv'     >> /etc/bash.bashrc
echo 'export PATH=/pyenv/bin:$PATH' >> /etc/bash.bashrc
echo 'eval "$(pyenv init -)"'       >> /etc/bash.bashrc

# INSTALL PYTHONS
echo 'export PYENV_ROOT=/pyenv'     >> /root/pyenv.sh
echo 'export PATH=/pyenv/bin:$PATH' >> /root/pyenv.sh
echo 'eval "$(pyenv init -)"'       >> /root/pyenv.sh

# 2.7.6
echo 'pyenv install 2.7.6'          >> /root/pyenv.sh

# 3.3.0
echo 'pyenv install 3.3.0'          >> /root/pyenv.sh

/bin/bash /root/pyenv.sh

# PERL
git clone git://github.com/tokuhirom/plenv.git /plenv
git clone git://github.com/tokuhirom/Perl-Build.git /plenv/plugins/perl-build/

echo 'export PATH=/plenv/bin:$PATH' >> /etc/bash.bashrc
echo 'eval "$(plenv init -)"'       >> /etc/bash.bashrc

# INSTALL PERLS
echo 'export PLENV_ROOT=/plenv'     >> /root/plenv.sh
echo 'export PATH=/plenv/bin:$PATH' >> /root/plenv.sh
echo 'eval "$(plenv init -)"'       >> /root/plenv.sh

# 5.18.0
echo 'plenv install 5.18.0'          >> /root/plenv.sh

# Install plenv
/bin/bash /root/plenv.sh
